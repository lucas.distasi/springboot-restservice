package com.distasilucas.springboot.restservices.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class ExceptionResponse {

    private Date time;
    private String message;
    private String details;

}
