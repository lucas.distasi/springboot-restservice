package com.distasilucas.springboot.restservices.repository;

import com.distasilucas.springboot.restservices.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPostRepository extends JpaRepository<Post, String> {
}
