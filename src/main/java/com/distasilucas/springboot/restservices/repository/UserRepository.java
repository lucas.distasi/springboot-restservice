package com.distasilucas.springboot.restservices.repository;

import com.distasilucas.springboot.restservices.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class UserRepository {
    private static final List<User> users = new ArrayList<>();

    static {
        users.add(new User("Adam", new Date()));
        users.add(new User("Joe", new Date()));
        users.add(new User("Jess", new Date()));
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) {
        User newUser = new User(user.getName(), user.getBirthDate());
        users.add(newUser);
        return newUser;
    }

    public User findById(String id) {
        for (User user : users) {
            if (user.getIdUser().toString().equals(id)) {
                return user;
            }
        }

        return null;
    }

    public User deleteById(String id) {
        Iterator<User> iterator = users.iterator();

        while (iterator.hasNext()) {
            User user = iterator.next();

            if (user.getIdUser().toString().equals(id)) {
                iterator.remove();
                return user;
            }
        }

        return null;
    }
}
