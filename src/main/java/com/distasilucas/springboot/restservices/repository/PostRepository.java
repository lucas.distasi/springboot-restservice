package com.distasilucas.springboot.restservices.repository;

import com.distasilucas.springboot.restservices.entity.Post;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostRepository {

    Multimap<String, Post> posts = ArrayListMultimap.create();

    public void savePost(String userId, Post post) {
        posts.put(userId, post);
    }

    public List<Post> findUserPosts(String idUser) {
        return new ArrayList<>(posts.get(idUser));
    }

}
