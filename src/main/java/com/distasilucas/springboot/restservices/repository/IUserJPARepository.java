package com.distasilucas.springboot.restservices.repository;

import com.distasilucas.springboot.restservices.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserJPARepository extends JpaRepository<User, String> {

}
