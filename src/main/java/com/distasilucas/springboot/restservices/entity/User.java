package com.distasilucas.springboot.restservices.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "User Details")
@Entity
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String idUser;

    @ApiModelProperty(required = true, notes = "Name should have at least 2 characters and no more than 30")
    @Size(min = 2, max = 30, message = "Name should have at least 2 characters and no more than 30")
    private String name;

    @ApiModelProperty(required = true, notes = "Birth date can not be in the future")
    @Past
    private Date birthDate;

    @OneToMany(mappedBy = "owner")
    private List<Post> posts;

    public User(String name, Date birthDate) {
        idUser = UUID.randomUUID().toString();
        this.name = name;
        this.birthDate = birthDate;
    }
}
