package com.distasilucas.springboot.restservices.resources;

import com.distasilucas.springboot.restservices.entity.Post;
import com.distasilucas.springboot.restservices.entity.User;
import com.distasilucas.springboot.restservices.exceptions.UserNotFoundException;
import com.distasilucas.springboot.restservices.repository.IPostRepository;
import com.distasilucas.springboot.restservices.repository.IUserJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(path = "/api-post/jpa")
public class PostJPAResource {

    private final IPostRepository iPostRepository;
    private final IUserJPARepository iUserJPARepository;

    @Autowired
    public PostJPAResource(IPostRepository postRepository, IUserJPARepository userJPARepository) {
        iPostRepository = postRepository;
        iUserJPARepository = userJPARepository;
    }

    @PostMapping(path = "/save")
    public ResponseEntity<Post> saveUser(@Valid @RequestBody Post post) {
        User user = iUserJPARepository.findById(post.getOwner().getIdUser()).orElse(new User());

        if (user.getIdUser() == null) {
            throw new UserNotFoundException(String.format("User with ID %s does not exists", post.getOwner().getIdUser()));
        }

        Post userPost = new Post(post.getPost(), post.getOwner());
        iPostRepository.save(userPost);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(userPost.getIdPost()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/{userId}/posts")
    public List<Post> getAllUserPosts(@PathVariable String userId) {
        User user = iUserJPARepository.findById(userId).orElse(new User());

        if (user.getIdUser() == null) {
            throw new UserNotFoundException(String.format("User with ID %s does not exists", userId));
        }

        return user.getPosts();
    }
}
