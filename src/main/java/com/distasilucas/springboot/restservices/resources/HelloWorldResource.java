package com.distasilucas.springboot.restservices.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@RestController
@RequestMapping(method = RequestMethod.GET, path = "/api")
public class HelloWorldResource {

    private final MessageSource messageSource;

    @Autowired
    public HelloWorldResource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @GetMapping(path = "/hello-world")
    public String helloWorld(@RequestHeader(value = "Accept-Language", required = false) Locale locale) {
        return messageSource.getMessage("hello.world.message", null, locale);
    }

    @GetMapping(path = "/hello-world-nh")
    public String helloWorldNoHeader() {
        return messageSource.getMessage("hello.world.message", null, LocaleContextHolder.getLocale());
    }

}
