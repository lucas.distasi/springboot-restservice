package com.distasilucas.springboot.restservices.resources;

import com.distasilucas.springboot.restservices.exceptions.UserNotFoundException;
import com.distasilucas.springboot.restservices.entity.Post;
import com.distasilucas.springboot.restservices.entity.User;
import com.distasilucas.springboot.restservices.repository.PostRepository;
import com.distasilucas.springboot.restservices.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api-user")
public class UserResource {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    private final String userNotExists = "User with ID %s does not exists";

    @Autowired
    public UserResource(UserRepository userRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @GetMapping(path = "/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping(path = "/user/{userId}")
    public EntityModel<User> findUser(@PathVariable String userId) {
        User user = userRepository.findById(userId);

        if (user == null) {
            throw new UserNotFoundException(String.format(userNotExists, userId));
        }

        EntityModel<User> resource = EntityModel.of(user);
        WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).getAllUsers());
        resource.add(link.withRel("allUsers"));

        return resource;
    }

    @PostMapping(path = "/save")
    public ResponseEntity<User> saveUser(@Valid @RequestBody User user) {
        User newUser = userRepository.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newUser.getIdUser()).toUri();
        return ResponseEntity.created(location).build();
    }

    @DeleteMapping(path = "/user/{userId}/remove")
    public void removeUser(@PathVariable String userId) {
        User user = userRepository.deleteById(userId);

        if (user == null) {
            throw new UserNotFoundException(String.format(userNotExists, userId));
        }
    }

    @PostMapping(path = "/user/{userId}/post")
    public void savePostFromUser(@RequestBody Post post, @PathVariable String userId) {
        User user = userRepository.findById(userId);

        if (user == null) {
            throw new UserNotFoundException(String.format(userNotExists, userId));
        }

        postRepository.savePost(userId, post);
    }

    @GetMapping(path ="/user/{userId}/posts")
    public List<Post> findAllUserPosts(@PathVariable String userId) {
        return postRepository.findUserPosts(userId);
    }
}
