package com.distasilucas.springboot.restservices.resources;

import com.distasilucas.springboot.restservices.entity.User;
import com.distasilucas.springboot.restservices.exceptions.UserNotFoundException;
import com.distasilucas.springboot.restservices.repository.IUserJPARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(path = "/api-user/jpa")
public class UserJPAResource {

    private final IUserJPARepository iUserJPARepository;

    @Autowired
    public UserJPAResource(IUserJPARepository userJPARepository) {
        iUserJPARepository = userJPARepository;
    }

    @GetMapping(path = "/users")
    public List<User> getAllUsers() {
        return iUserJPARepository.findAll();
    }

    @GetMapping(path = "/user/{userId}")
    public EntityModel<User> findUser(@PathVariable String userId) {
        User user = iUserJPARepository.findById(userId).orElse(new User());

        if (user.getIdUser() == null) {
            throw new UserNotFoundException(String.format("User with ID %s does not exists", userId));
        }

        EntityModel<User> resource = EntityModel.of(user);
        WebMvcLinkBuilder link = linkTo(methodOn(this.getClass()).getAllUsers());
        resource.add(link.withRel("allUsers"));

        return resource;
    }
}
